#Nombre del proyecto
TARGET = temp

#Archivos a compilar
OBJS  = main.o app_ints.o app_msps.o app_utils.o startup_stm32f072xb.o system_stm32f0xx.o
OBJS += list.o tasks.o queue.o timers.o port.o heap_1.o
OBJS += SEGGER_SYSVIEW.o SEGGER_RTT.o SEGGER_SYSVIEW_Conf.o SEGGER_SYSVIEW_FreeRTOS.o SEGGER_RTT_printf.o
OBJS += stm32f0xx_hal.o stm32f0xx_hal_cortex.o stm32f0xx_hal_pwr.o stm32f0xx_hal_rcc.o stm32f0xx_hal_rcc_ex.o stm32f0xx_hal_flash.o
OBJS += stm32f0xx_hal_gpio.o stm32f0xx_hal_uart.o stm32f0xx_hal_uart_ex.o stm32f0xx_hal_dma.o stm32f0xx_hal_rtc.o stm32f0xx_hal_wwdg.o
#archivo linker a usar
LINKER = linker.ld
#Simbolos gloobales del programa (#defines globales)
SYMBOLS = -DSTM32F072xB -DUSE_HAL_DRIVER
#directorios con archivos a compilar (.c y .s)
VPATH  = app
VPATH += cmsisf0/startups
VPATH += half0/Src
VPATH += rtos/source
VPATH += rtos/MemMang
VPATH += rtos/portable/ARM_CM0
VPATH += sysview/Config
VPATH += sysview/OS
VPATH += sysview/SEGGER

#direcotrios con archivos .h
INCLUDES  = -I app
INCLUDES  = -I app/config
INCLUDES += -I cmsisf0/core
INCLUDES += -I cmsisf0/registers
INCLUDES += -I half0/Inc
INCLUDES += -I rtos/include
INCLUDES += -I rtos/portable/ARM_CM0
INCLUDES += -I sysview/Config
INCLUDES += -I sysview/OS
INCLUDES += -I sysview/SEGGER

#compilador y opciones de compilacion
TOOLCHAIN = arm-none-eabi
CPU = -mcpu=cortex-m0 -mthumb -mfloat-abi=soft
CFLAGS = $(CPU) -Wall -g3 -Os -std=c99
CFLAGS += -ffunction-sections -fdata-sections
AFLAGS = $(CPU)
LFLAGS = $(CPU) -Wl,--gc-sections --specs=rdimon.specs --specs=nano.specs -Wl,-Map=Build/$(TARGET).map

#Instrucciones de compilacion
all : build $(TARGET)

$(TARGET) : $(addprefix Build/, $(TARGET).elf)
	$(TOOLCHAIN)-objcopy -Oihex $< Build/$(TARGET).hex
	$(TOOLCHAIN)-objdump -S $< > Build/$(TARGET).lst
	$(TOOLCHAIN)-size --format=berkeley $<

Build/$(TARGET).elf : $(addprefix Build/obj/, $(OBJS))
	$(TOOLCHAIN)-gcc $(LFLAGS) -T $(LINKER) -o $@ $^

Build/obj/%.o : %.c
	$(TOOLCHAIN)-gcc $(CFLAGS) $(INCLUDES) $(SYMBOLS) -o $@ -c $<

Build/obj/%.o : %.s
	$(TOOLCHAIN)-as $(AFLAGS) -o $@ -c $<

build:
	mkdir -p Build/obj

#borrar archivos generados
clean :
	rm -r Build

#RTT viewver to viuasualize print functions output 
terminal :
	#update path according to your Jlink installation packages
	"/c/Program Files (x86)/SEGGER/JLink/JLinkRTTClient"

#Programar al tarjeta
flash :
	#openocd -f interface/stlink-v2-1.cfg -f target/stm32f0x.cfg -c "program Build/$(TARGET).hex verify reset" -c shutdown
	openocd -f interface/jlink.cfg -c "transport select swd" -f target/stm32f0x.cfg -c "program Build/$(TARGET).hex verify reset" -c shutdown

#Conectar OpenOCD con al Tarjeta
open :
	#openocd -f interface/stlink-v2-1.cfg -f target/stm32f0x.cfg -c "reset_config srst_only srst_nogate"
	JLinkGDBServer -if SWD -device $(MCU) -nogui

#Lanzar sesion de debug (es necesario primero Conectar la tarjeta con OpenOCD)
debug :
	#$(TOOLCHAIN)-gdb Build/$(TARGET).elf -iex "set auto-load safe-path /"
	$(TOOLSET)-gdb Build/$(PROJECT).elf -iex "set auto-load safe-path /" -iex "target remote localhost:2331"

rmtemp:
	rm -f Build/$(TARGET).elf